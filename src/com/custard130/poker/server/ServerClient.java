package com.custard130.poker.server;

import java.net.InetAddress;

import com.custard130.sliskePoker.util.Player;

public class ServerClient {

	public String name;
	public InetAddress address;
	public int port;
	private final int ID;
	public int attempt = 0;
	
	private Player player;

	public ServerClient(String name, InetAddress address, int port, final int ID) {
		this.name = name;
		this.address = address;
		this.port = port;
		this.ID = ID;
		player = new Player(name);
	}
	
	public int getID() {
		return ID;
	}
	
	public Player getPlayer(){
		return player;
	}

}
