package com.custard130.poker;

import com.custard130.poker.game.Game;
import com.custard130.poker.server.Server;
import com.custard130.poker.server.ServerMain;

public class Main {
	private int port;
	private Server server;

	private Game game;

	public static void main(String[] args) {
		int port=8192;
		if (args.length != 1) {
			//System.out.println("Usage: java -jar ChernoChatServer.jar [port]");
			//return;
			port=8192;
		}
		//port = Integer.parseInt(args[0]);
		new ServerMain(port);
		Game game = new Game();
	}
}
