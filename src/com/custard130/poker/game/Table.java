package com.custard130.poker.game;

import java.util.ArrayList;
import java.util.Random;

public class Table {

	private static final int	TABLE_SIZE	= 6;

	private Random				random;

	private ArrayList<Player>	players;
	private Deck				cards;
	private Dealer				dealer;
	private ArrayList<Card>		tableCards;
	public int counter = 0;
	private MoneyPot pot;

	public Table() {
		random = new Random();
		dealer = new Dealer();
		players = new ArrayList<Player>();
		players.add(dealer);
		
		addPlayer(new Player("bob"));
		addPlayer(new Player("jeff"));
		addPlayer(new Player("tim"));
		addPlayer(new Player("vic"));
		addPlayer(new Player("jack"));

		
		tableCards = new ArrayList<Card>();
		pot = MoneyPot.initPot(this);
		reset();
	}
	
	public void reset(){
		counter++;
		for(Player p:players){
			p.reset();
		}
		cards = new Deck();
		cards.shuffle(random.nextInt(10));
		dealCards(2);
		tableCards.clear();
		
	}

	public boolean addPlayer(final Player p) {
		if (players.size() < TABLE_SIZE) {
			players.add(p);
			return true;
		}
		return false;

	}
	
	public void update(){
		if(!pot.isValid()){
			Game.giveWarning("Someone is trying to hack the client", true);
		}
		if(tableCards.size()==3){
			reset();
		}
		else if(dealer.isDone()){
			Card c = cards.nextCard();
			tableCards.add(c);
			//System.out.println(c);
			updateHands();
		}
		
	}

	public Player getPlayerAt(final int index) {
		if (index < players.size()) {
			return players.get(index);
		}
		return new Player(false);
	}

	private void updateHands(){
		for(Player p:players){
			p.updateHand(tableCards);
			if(p.getHand().checkForFullHouse()){
				Game.fullHouses++;
			}
		}
	}

	private void dealCards(final int cardsEach) {
		//System.out.println("dealing cards");
		for (int i = 0; i < cardsEach; i++) {
			for (Player p : players) {
				if (!(p instanceof Dealer))
					p.addCard(cards.nextCard());
			}
			dealer.addCard(cards.nextCard());
		}
		for (Player p : players) {
			p.initHand();
		}
		
	}
}
