package com.custard130.poker.game;

public class MoneyPot {
	private int contains;
	
	private boolean isValid = false;
	
	private MoneyPot(){
		contains=0;
	}
	private void init(){
		isValid=true;
	}
	public boolean makeBets(final int amt, final Player player){
		contains +=amt;
		return true;
	}
	
	public static MoneyPot initPot(Table table){
		MoneyPot temp = new MoneyPot();
		if(((Dealer)table.getPlayerAt(0)).isValid(temp)){
			temp.init();
		}
		return temp;
	}
	public boolean isValid(){
		return isValid;
	}
	public int getTotalMoneyBet(){
		return contains;
	}
}
