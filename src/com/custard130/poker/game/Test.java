package com.custard130.poker.game;

public class Test {

	Table table;
	
	public Test(Table t){
		table = t;
	}
	
	public void run(int count){
		while(count-->0){
			table.update();
		}
	}
	
}
