package com.custard130.poker.game;


public class Deck extends CardUtil {

	
	private Card[]	cards;
	private int		counter	= 0;

	public Deck() { // constructer, 1st thing called when this object is created
		setCards(); // calls my setCards method
		counter = 0; // makes sure counter is set to 0
	}

	private void setCards() {
		cards = new Card[52]; // sets my card array to a new array of 52 cards
							  // (array is basically a list)
		for (int i = 0; i < 52; i++) { // goes through the list 1 by 1
			cards[i] = new Card(getSuite(i), i % 13); // for each card it sets			
		}
	}

	private void showCardList() { // for testing, doesnt really matter
		for (Card c : cards) { // goes through each card in the list
			System.out.println(c); // displays that cards infomation
		}
	}

	public Card nextCard() {
		counter = counter % 62;
		return cards[counter++];
	}

	public void shuffle(final int times) {
		cards = shuffleCards(times, cards);
		counter = 0;
	}

}
