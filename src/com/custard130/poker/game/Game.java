package com.custard130.poker.game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class Game extends Canvas implements KeyListener {
	private static final long	serialVersionUID	= 1L;

	private Table				t;
	private JFrame				frame;
	private boolean				running				= false;
	private Test				tester;

	public static int			fullHouses			= 1;

	private static final long	START_TIME			= System.currentTimeMillis();

	public static void main(String[] args) {
		Game game = new Game();
		game.frame.setPreferredSize(new Dimension(900, 600));
		game.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		game.frame.setSize(900, 600);
		game.frame.setResizable(false);
		game.frame.setVisible(true);
		game.frame.setLocationRelativeTo(null);
		game.frame.requestFocus();
		game.frame.add(game);

	}

	public Game() {
		t = new Table();
		tester = new Test(t);
		frame = new JFrame("sliske poker");
		addKeyListener(this);
		//loop();

	}

	private void loop() {
		running = true;
		long currentTime;
		long timePassed;

		int counter = 100000;

		while ((counter--) > 0) {
			tester.run(4000);
			repaint();
		}

		// while (running) {
		// currentTime = System.currentTimeMillis();
		// timePassed = currentTime = START_TIME;
		// if ((timePassed % 1000) > 800) {
		// repaint();
		// }
		// }
	}

	public void paint(Graphics g) {
		frame.setTitle("sliske poker  - full houses : games - " + fullHouses
				+ " : " + t.counter + " - average 1 : "
				+ (t.counter / fullHouses));
		final int s0 = 290;
		final int s1 = 300;
		g.setColor(Color.black);
		for (int i = 0; i < 6; i++) {
			g.setColor(Color.black);
			int x = ((i % 3) * s1) + 3;
			int y = ((i / 3) * s1) + 3;
			// g.fillRect(x, y, s0, s0);
			// g.setColor(Color.white);
			Player p = t.getPlayerAt(i);
			// g.drawString(p.getName(), x + 25, y + 25);
			int line = 0;
			for (Card c : p.getCards()) {
				g.drawString(c.toString().toLowerCase(), x + 25, y + 50
						+ (line * 20));
				line++;
			}
			line += 3;
			Hand h = p.getHand();
			if (h != null && h.ready) {
				if (h.checkForFullHouse()) {
					g.drawString("full house", x + 25, y + 50 + (line * 20));
					fullHouses++;
					System.out.println("full house after " + t.counter
							+ " games");
					line++;
				} else {
					int pair = h.checkForPair();
					if (pair != -1) {
						g.drawString("pair of " + Card.getCardValue(pair + 1)
								+ "'s", x + 25, y + 50 + (line * 20));
						line++;
					}
					int pair2 = h.checkForPair(pair);
					if (pair2 != -1) {
						g.drawString("pair of " + Card.getCardValue(pair2 + 1)
								+ "'s", x + 25, y + 50 + (line * 20));
						line++;
					}
				}
			}

		}
	}

	private void test(int amt) {
		int counter = amt;

		while ((counter--) > 0) {
			tester.run(400);
			repaint();
		}repaint();
	}

	private void reset() {
		double temp = System.currentTimeMillis();
		t.reset();
		repaint();
		double timeTaken = System.currentTimeMillis() - temp;
		// System.out.println("rest took "+timeTaken+" milliseconds");
	}

	public static void giveWarning(final String message, boolean close) {
		System.err.println(message);
		if (close) {
			System.exit(ERROR);
		}
	}

	public void input(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			t.update();
			repaint();

		}
		if (e.getKeyCode() == KeyEvent.VK_P) {
			t.update();
			repaint();

		}
		if (e.getKeyCode() == KeyEvent.VK_T) {
			tester.run(300000);
			repaint();
		}
		if (e.getKeyCode() == KeyEvent.VK_E) {
			test(1000);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		input(e);
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
